const gulp = require('gulp');
const browserSync = require('browser-sync').create();

const sass = require('gulp-sass');
var fs = require('fs');
var path = require('path');
var merge = require('merge-stream');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');

var scssInput = './src';

var sassOptions = {
	errLogToConsole: true,
	outputStyle: 'expanded'
};

function getFolders(dir) {
	return fs.readdirSync(dir)
		.filter(function(file) {
			return fs.statSync(path.join(dir, file)).isDirectory();
		});
}

// Compile Sass
gulp.task('sass', function() {

	var processors = [
		autoprefixer({
			browsers: ['last 4 versions']
		})
	];

	var folders = getFolders(scssInput);
	
		var tasks = folders.map(function() {
			var src = path.join(scssInput, 'scss');
			var dst = path.join(scssInput, 'css');
	
			 return gulp.src(path.join(src, '*.scss'))
				.pipe(sass(sassOptions).on('error', sass.logError))
				.pipe(postcss(processors))
				.pipe(gulp.dest(dst))   
				.pipe(browserSync.stream()); 
		});
	
		return merge(tasks);

	// return gulp.src(['src/scss/*.scss'])
	// 	.pipe(sass(sassOptions).on('error', sass.logError))
	// 	.pipe(postcss(processors))
	// 	.pipe(gulp.dest('src/**/css'))
	// 	.pipe(browserSync.stream());
});

// Watch and Server
gulp.task('serve', ['sass'], function() {
	browserSync.init({
		server: './src'
	});

	gulp.watch(['src/scss/*.scss'], ['sass']);
	gulp.watch(['**/**.html']).on('change', browserSync.reload);
});

// Default
gulp.task('default', ['serve']);