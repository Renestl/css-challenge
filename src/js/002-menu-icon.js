document.addEventListener("DOMContentLoaded", function() {
	const hamburger = document.getElementsByClassName("hamburger");
	const active = document.getElementsByClassName("active");
	const animate = document.getElementsByClassName("noAnimate");

	hamburger[0].addEventListener('click', function() {
		hamburger[0].classList.toggle("active");
		
		for (i=0; i<animate.length; i++){
			animate[i].classList.remove("noAnimate");
		}
	});
});
